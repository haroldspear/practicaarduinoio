#include <stdio.h>    // Standard input/output definitions
#include <stdlib.h>
#include <string.h>   // String function definitions
#include <unistd.h>   // para usleep()
#include <getopt.h>
#include <math.h>

#include "arduino-serial-lib.h"

float calculateSD(float data[]);

void error(char* msg)
{
    fprintf(stderr, "%s\n",msg);
    exit(EXIT_FAILURE);
}

int main(int argc, char *argv[])
{
	int fd = -1;
	int baudrate = 9600;  // default

	fd = serialport_init("/dev/ttyACM0", baudrate);

	if( fd==-1 )
	{
		error("couldn't open port");
		return -1;
	}
	
	
	char a,b;	
	a='t';
	b='h';
	int temp=0, hum=0;
	serialport_flush(fd);

	int count=0,maxt=0,maxh=0,sumt=0,sumh=0;

	float mediat=0,mediah=0;

	int mint=9999,minh=9999;

	float temps[12];
	float hums[12];

	while(1){	
		write(fd,&a,1);
		read(fd,&temp,1);
		usleep(5000);//5 segundos

		write(fd,&b,1);	
		read(fd,&hum,1);

		printf("Temperatura: %d , Humedad: %d \n",temp,hum);
		
		if(temp<mint) mint=temp;				
		if(temp>maxt) maxt=temp;
		if(hum<minh) minh=hum;				
		if(hum>maxh) maxh=hum;

		sumt+=temp;
		sumh+=hum;
		temps[count]=temp;
		hums[count]=hum;

		if (count==11){
			//fuera del if esta count++, con -1 comenzara el while con 0
			count=-1;

			mediat=sumt/12;
			mediah=sumh/12;

			printf("Temperatura mínima: %d -",mint);
			printf("Temperatura maxima: %d -",maxt);
			printf("Temperatura promedio: %.2f -",mediat);
			printf("STD de la Temperatura: %.2f \n",calculateSD(temps));
	
			printf("Humedad mínima: %d -",minh);
			printf("Humedad maxima: %d -",maxh);
			printf("Humedad promedio: %.2f -",mediah);
			printf("STD de la Humedad: %.2f \n\n",calculateSD(hums));
			sumt=sumh=0;
			maxh=maxt=0;
			minh=mint=9999;
			
			printf("\nPress ctrl+z if you want exit...\n");
		}
		count++;	
			
	}			
	close( fd );
	return 0;	
}

/* Ejemplo para calcular desviacion estandar y media */
float calculateSD(float data[])
{
    float sum = 0.0, mean, standardDeviation = 0.0;

    int i;

    for(i = 0; i < 12; ++i)
    {
        sum += data[i];
    }

    mean = sum/10;

    for(i = 0; i < 12; ++i)
        standardDeviation += pow(data[i] - mean, 2);

    return sqrt(standardDeviation / 12);
}

